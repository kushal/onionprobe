# Onionprobe ChangeLog

## v1.0.0 - 2022-05-31

### Breaking changes

* Changed Prometheus exporter metric names to adhere to the
  [Best practices](https://prometheus.io/docs/practices/naming/) and to
  [other recommendations when writing an exporter](https://prometheus.io/docs/instrumenting/writing_exporters/#naming).
  Prometheus admins might want to rename their old metrics to the new
  ones to keep time series continuity, drop the old ones or keep both
  during a transition phase. The following metrics were renamed:
  * From `onionprobe_wait` to `onionprobe_wait_seconds`.
  * From `onion_service_latency` to `onion_service_latency_seconds`.
  * From `onion_service_descriptor_latency` to `onion_service_descriptor_latency_seconds`.
  * From `onion_service_fetch_error_counter` to `onion_service_fetch_error_total`.
  * From `onion_service_descriptor_fetch_error_counter` to
         `onion_service_descriptor_fetch_error_total`.
  * From `onion_service_request_exception` to `onion_service_request_exception_total`.
  * From `onion_service_connection_error` to `onion_service_connection_error_total`.
  * From `onion_service_http_error` to `onion_service_http_error_total`.
  * From `onion_service_too_many_redirects` to `onion_service_too_many_redirects_total`.
  * From `onion_service_connection_timeout` to `onion_service_connection_timeout_total`.
  * From `onion_service_read_timeout` to `onion_service_read_timeout_total`.
  * From `onion_service_timeout` to `onion_service_timeout_total`.
  * From `onion_service_certificate_error` to `onion_service_certificate_error_total`.

* Removed the `updated_at` label from all metrics, which was creating a new
  data series for every measurement on Prometheus.

* Removed the `hsdir` label from `onion_service_descriptor_reachable` metric,
  which was creating a new data series for every measurement on Prometheus.

### Features

* Monitoring node setup using Docker Compose and Prometheus, Alertmanager
  and Grafana dashboards served via Onion Services.

* Config generation improvements.

* New metrics:
  * `onion_service_fetch_requests_total`.
  * `onion_service_descriptor_fetch_requests_total`.
  * `onion_service_descriptor`, with Onion Service descriptor information.
  * `onion_service_probe_status`, with timestamp from the last test.

* Default Grafana Dashboard with basic metrics.

## v0.3.4 - 2022-05-11

### Fixes

* [x] Onionprobe's exporter port allocation conflict with the push gateway
      https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues/45

## v0.3.3 - 2022-05-11

### Fixes

* [x] Stem is unable to find cryptography module when runing from the pip package
      https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues/43

## v0.3.2 - 2022-05-11

Main issue: https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues/42

### Features

* [x] Enhanced config generators: switch all three config generators currently
      supporter (Real-World Onion Sites, SecureDrop and TPO) to rely on argparse
      for command line arguments.

## v0.3.1 - 2022-05-10

Main issue: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40717

### Features

* [x] Adds `packages/tpo.py` to generate an Onionprobe config with Tor
      Project's .onions.
      Details at https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/merge_requests/4

* [x] Other minor fixes and enhancements.

## v0.3.0 - 2022-04-19

Main issue: https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues/4

### Features

* [x] Debian package.
* [x] Better logging.
* [x] Additional command line options.
* [x] Handling of SIGTERM and other signals.

### Documentation

* [x] Manpage.
* [x] Auto-generate command line docs from CLI invocation.
* [x] Auto-generate manpage from `argparse`.

## v0.2.2 - 2022-04-06

### Fixes

* [x] Print usage when no arguments are supplied.

## v0.2.1 - 2022-04-06

### Fixes

* [x] Python package fixes.

## v0.2.0 - 2022-04-06

Main issue: https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues/3

### Enhancements

* [x] Python packaging: https://pypi.org/project/onionprobe.
* [x] Support for `--endpoints` command line argument.
* [x] Display available metrics at command line usage.
* [x] Adds `OnionprobeConfigCompiler` to help compile custom configuration.

## v0.1.0 - 2022-03-31

Main issue: https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues/2

### Meta

* [x] Move the repository to the [Onion Services Gitlab group](https://gitlab.torproject.org/tpo/onion-services).
* [x] Docstrings.
* [x] Environment variable controlling the configuration file to use.

### Probing

* [x] Set timeout at `get_hidden_service_descriptor()`.
* [x] Set timeout at `Requests`.
* [x] Set `CircuitStreamTimeout` in the built-in Tor daemon.
* [x] HTTPS certificate validation check/exception.
* [x] Max retries before throwing an error when getting descriptors.
      This could help answering the following questions:
    * [When an onion service lookup has failed at the first k HSDirs we tried, what are the chances it will still succeed?](https://gitlab.torproject.org/tpo/network-health/analysis/-/issues/28)
    * [What's the average number of hsdir fetches before we get the hsdesc?](https://gitlab.torproject.org/tpo/core/tor/-/issues/13208)
* [x] Max retries before throwing an error when querying the endpoint.

### Metrics

* [x] Status: sleeping, probing, starting or stopping.
* [x] Match found / not found.
* [x] Metric units in the description.
* [x] Number of introduction points.
* [x] Timestamp label.
* [x] Register HSDir used to fetch the descriptor.
      Check the [control-spec](https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/control-spec.txt)
      for `HSFETCH` command and the `HS_DESC` event ([using SETEVENTS](https://stem.torproject.org/tutorials/down_the_rabbit_hole.html)).
      Relevant issues:

### Enhancements

* [x] Refactor into smaller modules.
* [x] Better exception handling.

### Bonus

* [x] Script that compiles configuration from the
      [real-world-onion-sites](https://github.com/alecmuffett/real-world-onion-sites) repository.
* [x] Script that compiles configuration from the
      [the SecureDrop API](https://securedrop.org/api/v1/directory/).

## v0.0.1 - 2022-03-23

Main issue: https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues/1

### Basic

* [x] Take a list of onions to check and make sure that you can always fetch
      descriptors rather than just using cached descriptors etc.
* [x] Randomisation of timing to avoid systemic errors getting lucky and not
      detected.
* [x] Looping support: goes through the list of onions in a loop, testing one
      at a time continuously.
* [x] Flush descriptor caches so testing happens like if a fresh client.
* [x] Support for HTTP status codes.
* [x] Page load latency.
* [x] Ability to fetch a set of paths from each onion.
      Customisable by test path: not all our sites have content at the root,
      but do not bootstrap every time if that can be avoided.
* [x] Need to know about "does the site have useful content?"
      Regex for content inside the page: allow configuring a regex per path for
      what should be found in the returned content/headers.
* [x] Documentation.

### Meta

* [x] Dockerfile (and optionally a Docker Compose).

### Prometheus

* [x] Exports Prometheus metrics for the connection to the onion service, and
      extra metrics per path on the status code for each path returned by the server.
      If using the prometheus exporter with python, consider to just use request and
      beautiful soup to check that the page is returning what one expects.
* [x] Add in additional metrics wherever appropriate.
* [x] To get the timings right, the tool should take care of the test frequency and
      just expose the metrics rather than having Prometheus scraping individual
      targets on Prometheus' schedule.

### Bonus

* [x] Optionally launch it's [own Tor process](https://stem.torproject.org/api/process.html)
      like in [this example](https://stem.torproject.org/tutorials/to_russia_with_love.html#using-pycurl).
